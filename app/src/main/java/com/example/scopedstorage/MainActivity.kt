package com.example.scopedstorage

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenStarted
import com.example.scopedstorage.util.StoragePermissionsDelegate.Companion.getPermission
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.channels.actor

private const val TAG = "scopedstorageapp"
class MainActivity : AppCompatActivity() {

    private val viewModel : MainViewModel by viewModels()

    private val actor = lifecycleScope.actor<ItemClick> {
        for (click in channel) when(click) {
            is Click -> viewModel.open(click.position)
            is LongClick -> whenStarted { viewModel.delete(click.position) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val adapter = FilesAdapter(actor).also { recyclerView.adapter = it }
        viewModel.files.observe(this, Observer {  adapter.submitList(it) })
        lifecycleScope.launchWhenStarted {
            if (getPermission()) viewModel.browse(this@MainActivity)
        }
    }

    override fun onBackPressed() {
        if (viewModel.hasParent) viewModel.goBack()
        else super.onBackPressed()
    }
}
