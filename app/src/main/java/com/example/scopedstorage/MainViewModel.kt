package com.example.scopedstorage

import android.app.Application
import android.app.RecoverableSecurityException
import android.content.IntentSender
import android.os.Build
import android.util.Log
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.*
import com.example.scopedstorage.saf.getDocumentFiles
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val TAG = "scopedstorageapp"
class MainViewModel(application: Application) : AndroidViewModel(application) {

    val files : LiveData<MutableList<DocumentFile>> = MutableLiveData()
    private var _files : MutableList<DocumentFile> = mutableListOf()
    var current : DocumentFile? = null
    val hasParent : Boolean
        get() = current?.parentFile != null

    internal fun browse(activity: MainActivity) {
        Log.d(TAG, "browse")
//        viewModelScope.launch {
//            (files as MutableLiveData).value = withContext(Dispatchers.IO) {
//                val directory = Environment.getExternalStorageDirectory()
//                Log.d(TAG, "browsing ${directory?.path}")
//                directory?.listFiles()?.toList()
//            }
//        }
        activity.lifecycleScope.launchWhenStarted {
            _files = activity.getDocumentFiles()?.toMutableList() ?: return@launchWhenStarted
            (files as MutableLiveData).value = _files
        }
    }

    fun getFile(position: Int) = _files[position]

    suspend fun delete(position: Int): IntentSender? {
        return withContext(Dispatchers.IO) {
            try {
                val file = getFile(position)
                if (file.delete()) {
                    _files = _files.toMutableList()
                    _files.removeAt(position)
                    (files as MutableLiveData).postValue(_files)
                }
                null
            } catch (securityException: SecurityException) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    val recoverableSecurityException =
                        securityException as? RecoverableSecurityException
                            ?: throw RuntimeException(securityException.message, securityException)
                    recoverableSecurityException.userAction.actionIntent.intentSender
                } else {
                    throw RuntimeException(securityException.message, securityException)
                }
            }
        }
    }

    fun open(position: Int) = viewModelScope.launch {
        browse(getFile(position))
    }

    fun goBack() = viewModelScope.launch {
        val file = current?.parentFile ?: return@launch
        browse(file)
    }

    private suspend fun browse(file: DocumentFile) {
        _files = withContext(Dispatchers.IO) { file.listFiles().toMutableList() }
        current = file
        (files as MutableLiveData).postValue(_files)
    }
}