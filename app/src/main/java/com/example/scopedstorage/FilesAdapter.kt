package com.example.scopedstorage

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.documentfile.provider.DocumentFile
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.channels.SendChannel

private val DIFF = object : DiffUtil.ItemCallback<DocumentFile>() {
    override fun areItemsTheSame(oldItem: DocumentFile, newItem: DocumentFile) = oldItem.uri == newItem.uri

    override fun areContentsTheSame(oldItem: DocumentFile, newItem: DocumentFile) = true

}

class FilesAdapter(val actor: SendChannel<ItemClick>) : ListAdapter<DocumentFile, FilesAdapter.ViewHolder>(DIFF) {
    private lateinit var layoutInflater: LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (!::layoutInflater.isInitialized) layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_file, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val file = getItem(position)
        holder.title.text = file.name
        holder.icon.setImageResource(if (file.isDirectory) R.drawable.ic_folder else R.drawable.ic_file)

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById<TextView>(R.id.file_name)
        val icon: ImageView = itemView.findViewById<ImageView>(R.id.file_icon)
        init {
            itemView.setOnClickListener { actor.offer(Click(layoutPosition)) }
            itemView.setOnLongClickListener {
                actor.offer(LongClick(layoutPosition))
                true
            }
        }
    }
}

sealed class ItemClick
class Click(val position: Int) : ItemClick()
class LongClick(val position: Int) : ItemClick()