@file:Suppress("DEPRECATION")

package com.example.scopedstorage.saf

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.provider.DocumentsContract
import android.util.Log
import androidx.core.net.toUri
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.FragmentActivity
import com.example.scopedstorage.util.HeadlessFragment

private const val SAF_REQUEST = 85
private const val TAG = "SAFDelegate"

class SAFDelegate : HeadlessFragment<Uri?>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val safIntent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            safIntent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, Environment.getExternalStorageDirectory().toUri())
        }
        safIntent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        try {
            startActivityForResult(safIntent, SAF_REQUEST)
        } catch (e: ActivityNotFoundException) {
            exit()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        if (intent != null && requestCode == SAF_REQUEST) {
            intent.data?.let {
                PreferenceManager.getDefaultSharedPreferences(requireContext()).edit()
                    .putString("root", "$it").apply()
            }
            result.complete(intent.data)
        }
        else super.onActivityResult(requestCode, resultCode, intent)
        exit()
    }

    companion object {
    }
}

suspend fun FragmentActivity.getRoot() : Uri? {
    val fragment = SAFDelegate()
    supportFragmentManager.beginTransaction().add(fragment, "SAF").commitAllowingStateLoss()
    return fragment.result.await()
}

suspend fun FragmentActivity.getDocumentFiles(path: String? = null) : List<DocumentFile>? {
    val rootUri = PreferenceManager.getDefaultSharedPreferences(this).getString("root", null)?.let { Uri.parse(it) } ?: getRoot() ?: return null
    rootUri.persist(this)

    var documentFile = DocumentFile.fromTreeUri(this, rootUri)

    if (!path.isNullOrEmpty()) {
        val parts = path.substringAfterLast(':').split("/".toRegex()).dropLastWhile { it.isEmpty() }
        for (part in parts) {
            if (part == "") continue
            documentFile = documentFile?.findFile(part)
        }
    }

    if (documentFile == null) {
        Log.w(TAG, "Failed to find file")
        return null
    }

    // we have the end point DocumentFile, list the files inside it and return
    return documentFile.listFiles().filter { it.exists() && it.canRead() && it.name?.startsWith('.') == false }
}

private fun Uri.persist(context: Context) {
    val takeFlags: Int = Intent.FLAG_GRANT_READ_URI_PERMISSION
    context.contentResolver.takePersistableUriPermission(this, takeFlags)
}