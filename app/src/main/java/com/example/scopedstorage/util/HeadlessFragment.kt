package com.example.scopedstorage.util

import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CompletableDeferred

open class HeadlessFragment<T> : Fragment() {
    val result = CompletableDeferred<T>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    protected fun exit() {
        retainInstance = false
        activity?.run {
            if (!isFinishing) supportFragmentManager
                .beginTransaction()
                .remove(this@HeadlessFragment)
                .commitAllowingStateLoss()
        }
    }
}