package com.example.scopedstorage.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity

class StoragePermissionsDelegate : HeadlessFragment<Boolean>() {
    private var read = false
    private var write = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), 42)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode != 42) return
        for ((index, result) in grantResults.withIndex()) {
            if (result == PackageManager.PERMISSION_GRANTED) {
                when (permissions[index]) {
                    Manifest.permission.WRITE_EXTERNAL_STORAGE -> write = true
                    Manifest.permission.READ_EXTERNAL_STORAGE -> read = true
                }
            }
        }
        result.complete(read && write)
        exit()
    }

    companion object {
        suspend fun FragmentActivity.getPermission() : Boolean {
            if (canReadStorage() && canWriteStorage()) return true
            val fragment = StoragePermissionsDelegate()
            supportFragmentManager.beginTransaction()
                .add(fragment, "storage_perm")
                .commitAllowingStateLoss()
            return fragment.result.await()
        }
    }
}

fun Context.canReadStorage() = ContextCompat.checkSelfPermission(this,
    Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

fun Context.canWriteStorage() = ContextCompat.checkSelfPermission(this,
    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED